package dev.rehm;

import dev.rehm.models.Item;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionDriver {

    public static void main(String[] args) {
        Item item = new Item();
        Class<Item> itemClass = (Class<Item>) item.getClass();
        Field[] fields = itemClass.getDeclaredFields();
        for(Field f: fields){
            System.out.println(f.getName());
        }

        Method[] methods = itemClass.getMethods();
        for(Method m: methods){
            System.out.println(m);
        }
        try {
            Method m = itemClass.getMethod("setId",int.class);
            m.invoke(item, 5);
            System.out.println(item);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }


    }
}
